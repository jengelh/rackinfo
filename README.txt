rackinfo is a postprocessor for Dell PowerEdge Remote Admin Console
(RAC) monitoring which queries a set of computers, preferably rack
towers, and displays their environmental status according to their
location, either interactive or or web-based.
