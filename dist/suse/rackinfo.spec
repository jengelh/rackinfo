
Name:           rackinfo
Version:        2.07
Release:        0
Group:          System/Monitoring
URL:            http://jengelh.medozas.de/
Summary:        Dell RAC info postprocessor
License:        LGPL2

Source:         http://jengelh.hopto.org/f/%name/%name-%version.tbz2
BuildRoot:      %_tmppath/%name-%version-build
BuildRequires:  libHX-devel >= 1.7, wxWidgets-devel >= 2.7

%description
rackinfo is a postprocessor for Dell PowerEdge Remote Admin Console
(RAC) monitoring which queries a set of computers, preferably rack
towers, and displays their environmental status according to their
location, either interactive or or web-based.

Author(s):
----------
	Jan Engelhardt <jengelh [at] medozas de>

%prep
%setup

%build
%configure
make %{?jobs:-j%jobs};

%install
b="%buildroot";
rm -Rf "$b";
mkdir -p "$b";
make install DESTDIR="$b";

%clean
rm -Rf "%buildroot";

%files
%defattr(-,root,root)
/usr/lib/%name
%docdir %_docdir/%name
%_docdir/%name

%changelog
* Sat Jul 08 2006 - jengelh
- specfile update
- unicode-wx build
- revise constness
- add in-code doc
* Tue Nov 03 2005 - jengelh
- update to libHX 1.7.0
