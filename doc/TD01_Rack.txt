Rackinfo2: Technical Details
Jan Engelhardt
October 2004


RACK DEFINITIONS
================
topo/*.c

Each rack system (and its physical topology) is defined in a separate .c file
in the topo/ directory. These C files (as they imply) need to be compiled to
object code, and eventually shared object libraries (.so), which are then
loaded dynamically.

Each .so library must provide a function which returns the rack structure. As
such, the rack structure can (and should) be declared as static, i.e. only
available to the file.

    #include "rack.h"
    #include "rack_int.h"
    struct rack_t *setup(void);

These are the public members of the rack_t and racknode_t structures:

    typedef struct rack_struct {
        const char *name;
        racknode_t **row;
    };

    typedef struct cluster_node {
        char *name, *host_ip, *user, *password;
    };

Defining the physical layout is quite easy, thanks to the my design of it ;-)
First, we define rows of nodes:

    #include <stdio.h>
    ... // way more
    static racknode_t r03[] = {{""},        {"gwdl075"}, ..., {NULL}};
    static racknode_t r02[] = {{"gwdl092"}, {"gwdl074"}, ..., {NULL}};
    static racknode_t r01[] = {{"gwdl091"}, {"gwdl073"}, ..., {NULL}};

Here, only the canoncial name of the node is defined. All other members of a
single racknode_t structure are initialized to zero, as the ISO C standard
defines. Each row must end with a structure whose canoncial name (.name member)
is NULL.

You can also go by IP (or hostname), but do not forget that .name must be
filled in with something!

    static racknode_t r01[] = {
        {.name = "host91", .host_ip = "192.168.1.91"},
        {.name = "host73", .host_ip = "192.168.1.73"},
        {NULL},
    };

These definitions themselves only specify the order of nodes within a row, but
the ordering of rows is done in an extra variable to keep up the
afore-mentioned clear types:

    static racknode_t *my_rows[] = {r04, r03, r02, r01, NULL};

The left-most row will be displayed at the top.

setup() is called from the loader. It can post-initialize the rack structure
defined and which is to be returned. This might be necessary to save yourself
some typework. Assume that all machines only have the above-mentioned
(racknode_t r01[], r02[], etc.) name set, but they would still require a
username and password. You would do this:

    rack_t my_rack = { ... }; // please initialize

    rack_t *setup(void) {
        foreach_node_in_rack(&my_rack, node_setup, NULL);
        return &my_rack;
    }

    static void node_setup(racknode_t *node, rack_t *rack, void *__unused) {
        node->username = "root";
        node->password = "calvin";
    }

Do not forget to set node->host_ip which is used for connecting to the host,
node->name is only the CANONICAL (or mnemonic) NAME.


TOPOLOGY STRUCTURE FIXUP -- RACK
================================
load_racks() in loader.c

The rack structures are not complete yet, there is still the "private" members
left. rack->sensor_lock is a mutex used (qd.c:query_node()) to protect the
rack->sensor_tree from concurrent writes. rack->sensor_tree is an associative
array mapping sensor IDs, as retrieved by the RAC, to a sensor_t struct.

rack->sensor_array is an array of "sensor_t *" pointers. It is used for a
faster (and unsorted) traversal of the sensor_tree. rack->num_sensors specifies
the population count, rack->sensor_alloc the actual number of elements that
would fit into the array.

load_racks() also calculates the width and height of the rack topology since
gui.so needs this info for its object creation.

Every rack is pushed into the linked list "Racklist" whose defintion is
available through rack.h.


TOPOLOGY STRUCTURE FIXUP -- NODE
================================
fixup_node() in loader.c

Each node from the rack will have a .sensors member which is a mapping from
sensor id to the rack's sensor_t*.

The .tid and .bmap_idx members are property of qd.c and are needed to reap the
thread once it exited.


EOF
