
libHX
-----
RACKINFO uses libHX for a variety of purposes. The main one
is having ARB trees and Deques, the other is system
function independence (e.g. directory traversal) or common
operations.

http://jengelh.hopto.org/coding/libHX.php


wxWidgets (for gui.so)
----------------------
The GUI output subsystem uses wxWidgets as its base.
wxWindows 2.4 might work, wxWidgets (they did a name
change) 2.5 is recommended.

Because it is only a subsystem, it is not required to be
build or even used.
