#ifndef RACKINFO_QD_INT_H
#define RACKINFO_QD_INT_H 1

/* Maximum number of concurrent threads */
#define QD_MAX_THREADS    1

/* Wait this many seconds between querying... */
#define QD_ALL_WAIT_TIME  60 /* "all racks" */
#define QD_RACK_WAIT_TIME 0  /* single racks */
#define QD_NODE_WAIT_TIME 60 /* single nodes */
#define QD_BMAP_WAIT_TIME 1  /* between bmap position allocation failures */

#endif /* RACKINFO_QD_INT_H */
