/*
 *	rack.h - Include file for rack usage
 *	Objectives:
 *		- Defines rack_t, racknode_t
 */
#ifndef RACKINFO_RACK_H
#define RACKINFO_RACK_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <pthread.h>

struct HXbtree;
struct HXdeque;
struct rack;
struct sensor_props;

struct racknode {
	char *name, *host_ip, *username, *password;

	/* protected: */
	struct rack *parent;
	struct HXbtree /*<long>*/ *sensors;
	void *perl;
	pthread_t tid;
	char *bmap_pool;
	int bmap_idx;
};

struct rack {
	const char *name;
	struct racknode **row;

	/* protected: */
	pthread_mutex_t sensor_lock;
	struct HXbtree /*<struct sensor_props *>*/ *sensor_tree;
	struct sensor_props **sensor_array;
	size_t num_sensors, sensor_alloc, width, height;
};

/* Functions */
extern void foreach_node_in_rack(struct rack *,
	void (*callback)(struct racknode *, void *), void *);

/* Variables */
extern struct HXdeque /*<struct rack *>*/ *Racklist;

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* RACKINFO_RACK_H */
