#ifndef RACKINFO_PROTO_H
#define RACKINFO_PROTO_H 1

#ifdef __cplusplus
extern "C" {
#endif

struct rack;
struct racknode;
struct sensor_props;

/*
 *	COLOR.C
 */
extern void generate_color(const struct sensor_props *,
    double, double *, double *);

/*
 *	GATE.C
 */
extern char *sensors_string(struct racknode *);

/*
 *	QD.C
 */
extern void *qd_main(void *);

/*
 *	RACK.C
 */
extern struct rack *setup(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* RACKINFO_PROTO_H */
