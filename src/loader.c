/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	loader.c - Main initializer
 *	Objectives:
 *		- load all rack definitions (topo/ *.so)
 *		- load all subsystems (subsys_table)
 */
#include <sys/stat.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libHX/arbtree.h>
#include <libHX/deque.h>
#include <libHX/misc.h>
#include <libHX/option.h>
#include "loader.h"
#include "proto.h"
#include "rack.h"

// Functions
/*
static int start_subsystems(int, const char **, struct subsystem *);
static void wait_for_subsystems(struct subsystem *, int);
static int do_run(const char *, int);

static size_t load_racks(const char *); // rack related stuff
static void find_dimensions(struct rack *);
static void fixup_node(struct racknode *, struct rack *, void *);

static void getopt_flag_T(const struct HXoptcb *);
static void getopt_flag_X(const struct HXoptcb *);
static int get_options(int *, const char ***);
*/

/* External variables */
struct Optmap Opt = {.outfile = "/dev/stdout"};

/* Local variables */
static const char *const _dummy_args[] = {"-", NULL};
static struct subsystem subsys_table[] = {
	{.library = "gui.so",  .symbol = "entrypoint", .no_run = 1}, /* -X */
	{.library = NULL,      .symbol = NULL, .entry = qd_main},
	{.library = "html.so", .symbol = "entrypoint", .no_run = 1}, /* -T */
	{NULL},
};

static void *ep(void *arg)
{
	struct subsystem *sb = arg;

	return sb->entry(sb);
}

static int do_run(const char *lib, int x)
{
	/* Set a subsystem for load-and-run */
	struct subsystem *sp = subsys_table;
	while (sp->library != NULL || sp->entry != NULL) {
		if (sp->library != NULL && strcmp(sp->library, lib) == 0) {
			sp->no_run = !x;
			return 1;
		}
		++sp;
	}
	return 0;
}

/**
 * find_dimensions -
 * @rack:	rack to analyze
 *
 * gui.so needs the dimensions of the rack, so calculate it for them. I chose
 * not to ask the user who created user/xxx.c to give us the width / height
 * for that would be error prone.
 */
static void find_dimensions(struct rack *rack)
{
	struct racknode **trav = rack->row;

	while (*trav != NULL) {
		const struct racknode *node = *trav;
		size_t width = 0;

		while (node->name != NULL) {
			++width;
			++node;
		}
		if (width > rack->width)
			rack->width = width;
		++rack->height;
		++trav;
	}
	return;
}

/**
 * load_racks - load rack descriptions
 * @dir:	directory to look into
 *
 * Scan @dir for all *.so files and load them
 */
static size_t load_racks(const char *dir) {
	void *handle;
	const char *name;

	Racklist = HXdeque_init();
	if ((handle = HXdir_open(dir)) == NULL)
		return 0;

	while ((name = HXdir_read(handle)) != NULL) {
		struct rack *(*setup_func)(void);
		struct rack *rack;
		struct stat sb;
		size_t len;
		char buf[256];
		void *dh;

		snprintf(buf, sizeof(buf), "%s/%s", dir, name);
		if (stat(buf, &sb) != 0 || !S_ISREG(sb.st_mode) ||
		    ((len = strlen(buf)) > 3 &&
		    strcmp(buf + len - 3, ".so") != 0))
			continue;

		if ((dh = HX_dlopen(buf)) == NULL) {
			fprintf(stderr, "Error loading %s: %s\n",
			        buf, HX_dlerror());
			continue;
		}

		if ((setup_func = HX_dlsym(dh, "setup")) == NULL) {
			fprintf(stderr, "dlsym: %s\n", HX_dlerror());
			HX_dlclose(dh);
			continue;
		}

		printf("Loading %s\n", buf);
		if ((rack = setup_func()) == NULL) {
			fprintf(stderr, "Got a NULL pointer from setup()!\n");
			HX_dlclose(dh);
			continue;
		}

		/* Fixup private section */
		pthread_mutex_init(&rack->sensor_lock, NULL);
		rack->sensor_tree  = HXbtree_init(HXBT_MAP | HXBT_ICMP);
		rack->sensor_array = NULL;
		rack->sensor_alloc = 0;
		rack->num_sensors  = 0;

		foreach_node_in_rack(rack, fixup_node, rack);
		find_dimensions(rack);

		/* library needs to stay open */
		HXdeque_push(Racklist, rack);
	}

	HXdir_close(handle);
	return Racklist->itemcount;
}

static void fixup_node(struct racknode *node, void *ptr)
{
	node->parent   = ptr;
	node->sensors  = HXbtree_init(HXBT_MAP | HXBT_ICMP);
	node->perl     = NULL;
	node->tid      = 0;
	node->bmap_idx = -1;
}

int ffb_set(char *bmap)
{
	char *ptr = bmap;

	while (*ptr != '\0') {
		if (*ptr == '0') {
			*ptr = '1';
			return ptr - bmap;
		}
		++ptr;
	}
	return -1;
}

static void getopt_flag_T(const struct HXoptcb *cbi)
{
	do_run("html.so", 1);
}

static void getopt_flag_X(const struct HXoptcb *cbi)
{
	do_run("gui.so", 1);
}

static int get_options(int *argc, const char ***argv)
{
	static const struct HXoption options_table[] = {
		{.sh = 'T', .type = HXTYPE_NONE, .cb = getopt_flag_T,
		 .help = "Load the HTML output subsystem (also see -o)"},
		{.sh = 'O', .type = HXTYPE_NONE, .ptr = &Opt.qd_oneshot,
		 .help = "QD: Run through nodelist only once"},
		{.sh = 'X', .type = HXTYPE_NONE, .cb = getopt_flag_X,
		 .help = "Load the GUI output subsystem"},
		{.sh = 'o', .type = HXTYPE_STRING, .ptr = &Opt.outfile,
		 .help = "Output file for HTML output", .htyp = "FILE"},
		{.sh = 'v', .type = HXTYPE_NONE | HXOPT_INC, .ptr = &Opt.qd_verbose,
		 .help = "QD: Print a dot for each node that has been updated"},
		HXOPT_AUTOHELP,
		HXOPT_TABLEEND,
	};

	return HX_getopt(options_table, argc, argv, HXOPT_USAGEONERR) > 0;
}

/**
 * start_subsystem -
 *
 * Load and start subsystems. Also count the number of threads created so
 * we know when to exit the loader (which must wait).
 */
static int start_subsystems(int argc, const char **argv,
    struct subsystem *sp)
{
	int threads = 0;

	while (sp->library != NULL || sp->entry != NULL) {
		if (sp->library != NULL) {
			if (sp->no_run) {
				++sp;
				continue;
			}
			if ((sp->handle = HX_dlopen(sp->library)) == NULL) {
				fprintf(stderr, "Could not load %s: %s\n",
				        sp->library, HX_dlerror());
				++sp;
				continue;
			}
			if ((sp->entry = HX_dlsym(sp->handle, sp->symbol)) == NULL) {
				fprintf(stderr, "HX_dlsym() returned error: %s\n",
				        HX_dlerror());
				++sp;
				continue;
			}

			printf("Loading %s\n", sp->library);
		}

		/*
		 * If .library is NULL, entry is assmued to already point to an
		 * available function. (I.e. compiled-in.)
		 */

		sp->argc = argc;
		sp->argv = argv;
		if (sp->argc == 0) {
			/*
			 * get_options() replaces argc/argv with everything
			 * that was not an option. And if there were no
			 * non-options, argv is set to NULL.
			 * wxEntry() however crashes if argc/argv is 0/NULL.
			 */
			++sp->argc;
			sp->argv = _dummy_args;
		}

		pthread_create(&sp->id, NULL, ep, sp);
		++sp;
		++threads;
	}
	return threads;
}

static void wait_for_subsystems(struct subsystem *sp, int threads)
{
	/* Wait for the termination of all subsystems */
	while (threads > 0) {
		while (sp->library != NULL) {
			/*
			 * Subsystems set this variable when they are complete.
			 * That is because pthread_join() blocks if the thread
			 * has not yet exited.
			 */
			if (sp->exited) {
				pthread_join(sp->id, NULL);
				--threads;
				/* do not call join() again for this thread */
				sp->exited = false;
			}
			++sp;
		}
		sleep(1);
	}
	return;
}

int main(int argc, const char **argv)
{
	int threads = 0;

	if (!get_options(&argc, &argv))
		return EXIT_FAILURE;

	/* Loader does not do that much: */
	load_racks("topo");
	threads = start_subsystems(argc, argv, subsys_table);
	wait_for_subsystems(subsys_table, threads);
	return EXIT_SUCCESS;
}
