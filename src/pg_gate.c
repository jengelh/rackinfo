/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	gate.c - Perl gate to PowerEdge::RAC
 *
 *	Objectives:
 *	  - Embed the Perl engine
 *	  - run sensors.pl:get_sensors()
 */
#include <pthread.h>
#include <stdio.h>
#include <libHX.h>
#define HAS_BOOL 1
#include <EXTERN.h>
#include <perl.h>
#include "proto.h"
#include "rack.h"

/* External functions */
EXTERN_C void xs_init(pTHX);

/* Local functions */
static void cnode_perl_init(struct racknode *);
static void cnode_perl_free(struct racknode *);
static char *call_sensors(pTHX_ const char *, const char *, const char *);

/* Local variables */
static pthread_mutex_t perlinit_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 * sensors_string -
 * @node:	node to query
 *
 * Initializes the node's perl interpreter, if necessary, and then calls out
 * to perlspace.
 */
char *sensors_string(struct racknode *node)
{
	PerlInterpreter *interp;

	if (node->perl == NULL)
		cnode_perl_init(node);

	interp = node->perl;
	PERL_SET_CONTEXT(interp);
	return call_sensors(interp, node->host_ip,
	       node->username, node->password);
}

/**
 * cnode_perl_init -
 * @cnode:	pointer to rack node
 *
 * Initialize a perl interpreter for each rack node, for reasons of
 * parallelism. SNMP querying is painfully slow enough. (I really did not want
 * to rewrite PowerEdge::RAC to C.)
 */
static void cnode_perl_init(struct racknode *cnode)
{
	const char *args[] = {"-", "sensors.pl", NULL};
	PerlInterpreter *my_perl;

	cnode->perl = my_perl = perl_alloc();
	PERL_SET_CONTEXT(my_perl);
	perl_construct(my_perl);
	PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
	pthread_mutex_lock(&perlinit_lock);
	perl_parse(my_perl, xs_init, 2, args, NULL);
	perl_run(my_perl);
	pthread_mutex_unlock(&perlinit_lock);
}

/**
 * cnode_perl_free -
 * @cnode:	pointer to rack node
 */
static void cnode_perl_free(struct racknode *cnode)
{
	PerlInterpreter *ip = cnode->perl;
	cnode->perl = NULL;
	PERL_SET_CONTEXT(ip);
	perl_destruct(ip);
	perl_free(ip);
}

/**
 * call_sensors -
 * @pTHX:	perl thread
 * @host:	host to query
 * @user:	user to connect as
 * @password:	password to use
 *
 * Run get_sensors() function from PowerEdge/RAC.pm using the given
 * parameters. The return value from get_sensors() is saved (malloc'ed) and
 * returned.
 */
static char *call_sensors(pTHX_ const char *host, const char *user,
    const char *password)
{
	dSP;
	char *result;
	SV *scalar;
	int count;

	ENTER;
	SAVETMPS;

	PUSHMARK(SP);
	XPUSHs(sv_2mortal(newSVpv(host, 0)));
	XPUSHs(sv_2mortal(newSVpv(user, 0)));
	XPUSHs(sv_2mortal(newSVpv(password, 0)));
	PUTBACK;

	count = call_pv("get_sensors", G_SCALAR);

	SPAGAIN;
	if (count != 1)
		croak("Big trouble\n");

	scalar = POPs;
	result = HX_strdup(SvPV_nolen(scalar));
	PUTBACK;
	FREETMPS;
	LEAVE;
	return result;
}
