#!/usr/bin/perl
#==============================================================================
# rackinfo - RAC monitoring
#   Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2004 - 2006
#   -- License restrictions apply (GPL v2)
#
#   This file is part of rackinfo.
#   rackinfo is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by
#   the Free Software Foundation; however ONLY version 2 of the License.
#
#   rackinfo is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program kit; if not, write to:
#   Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
#   Boston, MA  02110-1301  USA
#
#   -- For details, see the file named "LICENSE.GPL2"
#
# sensors.pl - RAC queryer
# Objectives:
#   - Send HTTP request to the RAC chip
#   - Return an easy parseable string (remember, we embed from C)
#     with all the sensor stuff
#==============================================================================
use lib ".";
use strict;
use PowerEdge::RAC;

sub get_sensors {
    my($host, $user, $pass) = @_;
    my $server = new PowerEdge::RAC(
      host => $host, user => $user, password => $pass);
    my $rv;

    eval {
        if(!$server->is_powered_on()) {
            return 0;
        }
        $rv = $server->rd_sensors();
    } || print STDERR $@;
    return $rv;
}

# Additional functions to the PowerEdge::RAC package.
# Unaffected by and does not affect the 'use' statement above.
#
package PowerEdge::RAC;

sub rd_sensors {
    my($self, @sensor_codes) = @_;
    my $href = $self->read_sensors(@sensor_codes);
    my $s;

    foreach my $sensor_id (keys %$href) {
        my $sref = _interpret_codes($href->{$sensor_id});
        foreach my $k (keys %$sref) {
            $s .= "$sensor_id $k ".$sref->{$k}."\n";
        }
    }

    return $s;
}

#==============================================================================
