/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */
#include <stdio.h>
#include <libHX.h>
#include "rack.h"

/* External variables */
struct HXdeque *Racklist;

void foreach_node_in_rack(struct rack *rack,
    void (*callback)(struct racknode *, void *), void *ptr)
{
	struct racknode **row;

	for (row = rack->row; *row != NULL; ++row) {
		struct racknode *node;

		for (node = *row; node->name != NULL; ++node) {
			if (*node->name == '\0')
				continue;
			callback(node, ptr);
		}
	}
}
