#ifndef RACKINFO_COLOR_H
#define RACKINFO_COLOR_H 1

extern void generate_color(const struct sensor_props *,
	double, double *, double *);

#endif /* RACKINFO_COLOR_H */
