#ifndef RACKINFO_SENSOR_H
#define RACKINFO_SENSOR_H 1

#ifdef __cplusplus
extern "C" {
#endif

enum sensor_type {
	T_NONE = 0,
	T_TEMPERATURE,
	T_FAN,
	T_VOLTAGE,
	T_OTHER,
};

struct sensor_props {
	char *name, *type_str, *unit;
	double lo_crit, lo_warn, hi_warn, hi_crit;
	union {
		unsigned long sid;
		void *sid_v;
	};
	enum sensor_type type;
};

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* RACKINFO_SENSOR_H */
