/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	Objectives:
 *		- Contact each node in each rack for sensor info
 *		- Store available sensors in a per-rack variable
 *		- Actual sensor values are stored per-node
 */
#include <sys/select.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libHX.h>
#include "loader.h"
#include "proto.h"
#include "qd_int.h"
#include "proto.h"
#include "rack.h"
#include "sensor.h"

struct qd_pool {
	pthread_mutex_t tid_pool_lock;
	char tid_pool[QD_MAX_THREADS + 1];
};

static void qd_spawn_query(struct racknode *node, void *ptr)
{
	struct qd_pool *pool = ptr;
	struct timeval waiting;

	if (node->bmap_idx >= 0)
		/* This node already has a thread */
		return;

	node->bmap_pool = pool->tid_pool;
	pthread_mutex_lock(&pool->tid_pool_lock);
	while ((node->bmap_idx = ffb_set(pool->tid_pool)) < 0) {
		pthread_mutex_unlock(&pool->tid_pool_lock);
		waiting.tv_sec  = QD_BMAP_WAIT_TIME;
		waiting.tv_usec = 0;
		select(0, NULL, NULL, NULL, &waiting);
		pthread_mutex_lock(&pool->tid_pool_lock);
	}
	pthread_mutex_unlock(&pool->tid_pool_lock);
	pthread_create(&node->tid, NULL, qd_do_query, node);
	pthread_detach(node->tid);

	if (QD_NODE_WAIT_TIME != 0) {
		waiting.tv_sec  = QD_NODE_WAIT_TIME;
		waiting.tv_usec = 0;
		select(0, NULL, NULL, NULL, &waiting);
	}

	return;
}

void *qd_main(void *argp)
{
	struct subsystem *sp = argp;
	struct timeval waiting;
	struct qd_pool pool;

	memset(pool.tid_pool, '0', sizeof(pool.tid_pool));
	pool.tid_pool[QD_MAX_THREADS] = '\0';

	while (true) {
		const struct HXdeque_node *row_ptr = Racklist->first;

		while (row_ptr != NULL) {
			struct rack *rack = row_ptr->ptr;
			foreach_node_in_rack(rack, qd_spawn_query, &pool);
			row_ptr = row_ptr->Next;

			if (QD_RACK_WAIT_TIME == 0)
				continue;

			waiting.tv_sec  = QD_RACK_WAIT_TIME;
			waiting.tv_usec = 0;
			select(0, NULL, NULL, NULL, &waiting);
		}

		waiting.tv_sec  = QD_ALL_WAIT_TIME;
		waiting.tv_usec = 0;
		select(0, NULL, NULL, NULL, &waiting);
		if (Opt.qd_oneshot)
			break;
	}

	++sp->exited;
	return NULL;
}

static void *qd_do_query(void *arg)
{
#define UPDATE_DBL(str, ptr) \
	if(*(ptr) == -1 && strcasecmp(key, (str)) == 0) \
		*(ptr) = strtod(value, NULL);
#define UPDATE_STR(str, ptr) \
	if(*(ptr) == NULL && strcasecmp(key, (str)) == 0) \
		*(ptr) = HX_strdup(value);

	struct racknode *node = arg;
	struct rack *rack = node->parent;
	char *info_original, *info, *infop;

	info_original = info = sensors_string(node);

	while ((infop = HX_strsep(&info, "\n")) != NULL) {
		unsigned long sid;
		struct sensor_props *props;
		char *key, *value;
		void *sid_v;

		/* Update a node sensors's value */
		if ((key = strchr(infop, ' ')) == NULL)
			continue;
		*key++ = '\0';
		if ((value = strchr(key, ' ')) == NULL)
			continue;
		*value++ = '\0';

		/* Now we have three fields: infop key value */
		sid   = strtoul(infop, NULL, 0);
		sid_v = (void *)sid;
		if (strcmp(key, "VAL") == 0) {
			if (strpbrk(value, ".,") != NULL)
				/*
				 * Looks like it is a double. Currently, only
				 * voltage levels are reported as doubles, so
				 * it is safe to multiply by 1000 and then
				 * convert it to integer. That way we keep some
				 * precision.
				 */
				HXbtree_add(node->sensors, sid_v,
					reinterpret_cast(void *,
					static_cast(long,
					1000 * strtod(value, NULL))));
			else
				/*
				 * Integers which are not bigger than the size
				 * of a CPU register can be converted without
				 * extra design.
				 */
				HXbtree_add(node->sensors, sid_v,
					reinterpret_cast(void *,
					strtol(value, NULL, 0)));
		}

		/*
		 * Add the sensor to the global table if it is
		 * not already there.
		 */
		pthread_mutex_lock(&rack->sensor_lock);
		if ((props = HXbtree_get(rack->sensor_tree, sid_v)) == NULL) {
			if ((props = malloc(sizeof(struct sensor_props))) == NULL) {
				perror("malloc");
				abort();
			}
			props->name     =
			props->type_str =
			props->unit     = NULL;
			props->lo_crit  =
			props->lo_warn  =
			props->hi_warn  =
			props->hi_crit  = -1;
			props->sid      = sid;
			props->type     = T_NONE;
			HXbtree_add(rack->sensor_tree, sid_v, props);
			push_sa(rack, props);
		}

		if (props->type_str == NULL &&
		    strcasecmp(key, "SENSOR_TYPE") == 0) {
			props->type_str = HX_strdup(value);
			if (strcasecmp(value, "Temperature") == 0)
				props->type = T_TEMPERATURE;
			else if (strcasecmp(value, "Fan") == 0)
				props->type = T_FAN;
			else if (strcasecmp(value, "Voltage") == 0)
				props->type = T_VOLTAGE;
			else
				props->type = T_OTHER;
		}

		UPDATE_STR("NAME",               &props->name);
		UPDATE_STR("UNITS",              &props->unit);
		UPDATE_DBL("LOW_CRITICAL",       &props->lo_crit);
		UPDATE_DBL("LOW_NON_CRITICAL",   &props->lo_warn);
		UPDATE_DBL("UPPER_NON_CRITICAL", &props->hi_warn);
		UPDATE_DBL("UPPER_CRITICAL",	 &props->hi_crit);
		pthread_mutex_unlock(&rack->sensor_lock);
	}

	node->bmap_pool[node->bmap_idx] = '0';
	node->bmap_idx = -1;
	free(info_original);

	if (Opt.qd_verbose) {
		printf(".");
		fflush(stdout);
	}

	return NULL;
#undef UPDATE_INT
#undef UPDATE_STR
}

static void push_sa(struct rack *rack, struct sensor_props *ptr)
{
	/*
	 * The Sensors_array is provided to use [] traversal which is faster
	 * than B-tree trav.
	 */
	if (rack->sensor_alloc <= rack->num_sensors) {
		if(rack->sensor_alloc == 0)
			++rack->sensor_alloc;
		rack->sensor_alloc *= 2;
		rack->sensor_array  = realloc(rack->sensor_array,
			sizeof(struct sensor_props *) * rack->sensor_alloc);
		if (rack->sensor_array == NULL) {
			perror("malloc");
			abort();
		}
	}

	rack->sensor_array[rack->num_sensors++] = ptr;
}
