/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	mainwindow.cpp - GUI output subsystem
 *	Objectives:
 *		- Create top-level window and a notebook
 *		- Create a notebook tab for each rack
 */
#include <cstdio>
#include <libHX.h>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#	include <wx/wx.h>
#endif
#include <wx/statline.h>
#include "loader.h"
#include "rack.h"
#include "gui/guiglobal.hpp"
#include "gui/mainwindow.hpp"
#include "gui/rack.hpp"

void *entrypoint(void *argp)
{
	struct subsystem *cpp = static_cast<struct subsystem *>(argp);
	void *rv = reinterpret_cast<void *>(wxEntry(cpp->argc,
	           const_cast<char **>(cpp->argv)));
	++cpp->exited;
	return rv;
}

class MyApp : public wxApp {
    public:
	bool OnInit(void);
};

IMPLEMENT_APP_NO_MAIN(MyApp);

bool MyApp::OnInit(void)
{
	MainWindow *mw = new MainWindow(wxT("Rackinfo"), wxDPOS, wxSize(700, 700));
	SetTopWindow(mw);
	mw->Show(true);
	return true;
}

BEGIN_EVENT_TABLE(MainWindow, wxFrame)
//	EVT_BUTTON(wxID_EXIT, MainWindow::CB_Exit)
END_EVENT_TABLE()

MainWindow::MainWindow(const wxString &title, const wxPoint &pos,
    const wxSize &size) :
	wxFrame(NULL, wxID_ANY, title, pos, size)
{
	int widths[] = {-1, 80};

	CreateStatusBar(2);
	SetStatusWidths(2, widths);
	SetStatusText(wxT("v2.05"), 1);

	wxBoxSizer *vpacker  = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *hpacker  = new wxBoxSizer(wxHORIZONTAL);
	wxNotebook *notebook = new wxNotebook(this, wxID_ANY);

	for (const struct HXdeque_node *travp = Racklist->first;
	     travp != NULL; travp = travp->next)
	{
		struct rack *rack = static_cast<struct rack *>(travp->ptr);
		notebook->AddPage(new Rack(notebook, rack), fU8(rack->name));
	}

	vpacker->Add(notebook, 1, wxGROW | wxALL, 5);

	/* Master pack */
	vpacker->Add(new wxStaticLine(this, wxID_ANY, wxDPOS, wxDSIZE, wxHORIZONTAL), 0, wxGROW | wxTOP | wxBOTTOM, 5);
	vpacker->Add(hpacker, 0, wxALIGN_RIGHT);
	hpacker->Add(new wxButton(this, wxID_EXIT, _("&Close")), 0, wxALL, 3);

	SetSizer(vpacker);
}

void MainWindow::CB_Exit(wxCommandEvent &event)
{
	exit(0);
}
