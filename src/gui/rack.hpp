/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	rack.hpp - Include file for "Rack", a graphical element
 *	Objectives:
 *		- Class definition for Rack and PanelMOV
 */
#ifndef RACKINFO_GUI_RACK_HPP
#define RACKINFO_GUI_RACK_HPP 1

#include <cstdio>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#	include <wx/wx.h>
#endif
#include <wx/notebook.h>
#include "gui/mainwindow.hpp"

class Rack;
class PanelMOV;
struct rack;
struct racknode;
struct sensor_props;

/*
 * The "Rack" class defines a wxPanel which contains all info of a given rack.
 * This panel is meant to be put into a wxNotebook for easy display.
 */
class Rack : public wxPanel {

    public:
	Rack(wxNotebook *, struct rack *);
	void CB_UpdateNodeinfo(wxMouseEvent &);

    public:
	wxStaticText *tx_nodeinfo;

    protected:
	void CB_SelectSensor(wxCommandEvent &);
	void CB_Timer(wxTimerEvent &);
	void CB_UpdateSensors(wxCommandEvent &);
	void CB_UpdateGrid(wxCommandEvent &);
	void set_color(wxPanel *, wxStaticText *, double,
		const struct sensor_props *);
	void update_cell(struct racknode *, void *, size_t);
	void update_grid(void);
	int update_sensor_list(void);

    protected:
	enum {
		ID_SELECT = 1,
		ID_RUNTIMER,
		ID_USENSOR,
		ID_UGRID,
	};

	wxListBox *listbox;
	wxTimer *timer;
	wxStaticText **grid_tx, *tx_sensorinfo;
	wxBoxSizer **grid_bx;
	PanelMOV **grid_pa;
	struct rack *rack;

    private:
	DECLARE_EVENT_TABLE();
};

/*
 * This extra class is a derivement of wxPanel to add background color
 * and tooltips to the individual cells.
 */
class PanelMOV : public wxPanel {

    public:
	PanelMOV(wxPanel *, Rack * = NULL, wxWindowID = wxID_ANY);

	const char *nodename;

    protected:
	void CB_EnterWindow(wxMouseEvent &);

	Rack *rack_wd;

    private:
	DECLARE_EVENT_TABLE();
};

#endif /* RACKINFO_GUI_RACK_HPP */
