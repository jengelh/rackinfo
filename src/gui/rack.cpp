/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	rack.cpp - "Rack" graphical element
 *	Objectives:
 *		- Create a wxPanel which contains all elements relevant
 *		for a rack
 *		- Listbox for sensor selection and a grid display
 *		- Grid display uses a PanelMOV for each cell
 */
#include <csignal>
#include <cstdio>
#include <libHX.h>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#	include <wx/wx.h>
#endif
#include <wx/notebook.h>
#include <wx/statline.h>
#include <wx/splitter.h>
#include "loader.h"
#include "proto.h"
#include "rack.h"
#include "sensor.h"
#include "gui/mainwindow.hpp"
#include "gui/rack.hpp"

BEGIN_EVENT_TABLE(Rack, wxPanel)
	EVT_LISTBOX(ID_SELECT, Rack::CB_SelectSensor)
	EVT_TIMER(ID_RUNTIMER, Rack::CB_Timer)
	EVT_BUTTON(ID_USENSOR, Rack::CB_UpdateSensors)
	EVT_BUTTON(ID_UGRID,   Rack::CB_UpdateGrid)
END_EVENT_TABLE()

Rack::Rack(wxNotebook *parent, struct rack *arg_rack) :
	wxPanel(parent)
{
	/* Other init */
	rack = arg_rack;

	/* Splitter window */
	wxBoxSizer *vpacker = new wxBoxSizer(wxVERTICAL);

	wxSplitterWindow *spw = new wxSplitterWindow(this, wxID_ANY);
	wxPanel *left_panel, *right_panel;
	wxBoxSizer *left_sizer, *right_sizer;

	left_panel = new wxPanel(spw, wxID_ANY);
	left_sizer = new wxBoxSizer(wxVERTICAL);
	left_panel->SetSizer(left_sizer);
	left_panel->SetAutoLayout(true);

	right_panel = new wxPanel(spw, wxID_ANY);
	right_sizer = new wxBoxSizer(wxVERTICAL);
	right_panel->SetSizer(right_sizer);
	right_panel->SetAutoLayout(true);

	/* Left side */
	listbox = new wxListBox(left_panel, ID_SELECT, wxDPOS, wxDSIZE,
		0, NULL, wxLB_SINGLE | wxLB_NEEDED_SB | wxLB_SORT);

	left_sizer->Add(listbox, 1, wxGROW);
	spw->SplitVertically(left_panel, right_panel, 200);

	/* Right side */
	wxGridSizer *right_grid = new wxGridSizer(rack->height, rack->width, 0, 0);
	right_sizer->Add(right_grid, 0, wxALIGN_CENTER, 3);

	/* Button bar */
	wxBoxSizer *hpacker = new wxBoxSizer(wxHORIZONTAL);
	tx_sensorinfo = new wxStaticText(this, wxID_ANY, wxEmptyString);
	tx_nodeinfo   = new wxStaticText(this, wxID_ANY, wxT("(no node)"),
		wxDPOS, wxDSIZE, wxALIGN_RIGHT);

	hpacker->Add(new wxButton(this, ID_USENSOR, wxT("Update &sensors")),
		0, wxALIGN_CENTER | wxALL, 3);
	hpacker->Add(new wxButton(this, ID_UGRID, wxT("Update &grid")),
		0, wxALIGN_CENTER | wxALL, 3);
	hpacker->Add(new wxStaticLine(this, wxID_ANY, wxDPOS, wxDSIZE,
		wxLI_VERTICAL), 0, wxGROW | wxLEFT | wxRIGHT, 3);
	hpacker->Add(tx_sensorinfo, 0, wxALIGN_CENTER | wxALL, 3);
	hpacker->Add(new wxStaticLine(this, wxID_ANY, wxDPOS, wxDSIZE,
		wxLI_VERTICAL), 0, wxGROW | wxLEFT | wxRIGHT, 3);
	hpacker->Add(tx_nodeinfo, 0, wxALIGN_CENTER | wxALL, 3);

	/* Master pack */
	vpacker->Add(spw, 1, wxGROW);
	vpacker->Add(new wxStaticLine(this, wxID_ANY, wxDPOS, wxDSIZE,
		wxHORIZONTAL), 0, wxGROW | wxTOP | wxBOTTOM, 3);
	vpacker->Add(hpacker, 0, wxALIGN_LEFT);
	SetSizer(vpacker);

	/* Grid */
	wxFont cell_font(14, wxFONTFAMILY_DEFAULT, 0, 0);
	size_t s = rack->width * rack->height;

	grid_pa = new PanelMOV *[s];
	grid_bx = new wxBoxSizer *[s];
	grid_tx = new wxStaticText *[s];

	for (size_t i = 0; i < s; ++i) {
		grid_pa[i] = new PanelMOV(right_panel, this, wxID_ANY);
		grid_bx[i] = new wxBoxSizer(wxVERTICAL);
		grid_tx[i] = new wxStaticText(grid_pa[i], wxID_ANY, wxT("--"));

		grid_tx[i]->SetFont(cell_font);
		grid_bx[i]->Add(grid_tx[i], 0, wxALIGN_CENTER | wxALL, 3);
		grid_pa[i]->SetSizer(grid_bx[i]);
		right_grid->Add(grid_pa[i], 0, wxALIGN_CENTER);
	}

	/* Other */
	timer = new wxTimer(this, ID_RUNTIMER);
	timer->Start(Opt.interval);
}

void Rack::CB_SelectSensor(wxCommandEvent &event)
{
	/* Clicked on an element in the listbox */
	size_t i = listbox->GetSelection();

	if (i < 0) {
		tx_nodeinfo->Update();
		update_grid();
		return;
	}

	void *sid = listbox->GetClientData(i);
	const struct sensor_props *props =
		HXbtree_get<const struct sensor_props *>
		(rack->sensor_tree, sid);

	if (props != NULL) {
		wxString tmp;

		if (props->type == T_VOLTAGE) {
			tmp.Printf(wxT("%s: %.2f/%.2f/%.2f/%.2f %s"),
				props->name, props->lo_crit, props->lo_warn,
				props->hi_warn, props->hi_crit, props->unit);
		} else {
			tmp.Printf(wxT("%s: %d/%d/%d/%d %s"),
				props->name, static_cast<int>(props->lo_crit),
				static_cast<int>(props->lo_warn),
				static_cast<int>(props->hi_warn),
				static_cast<int>(props->hi_crit), props->unit);
		}
		tx_sensorinfo->SetLabel(tmp);
	}

	tx_nodeinfo->Update();
	update_grid();
}

void Rack::CB_Timer(wxTimerEvent &event)
{
	update_sensor_list();
	update_grid();
}

void Rack::CB_UpdateSensors(wxCommandEvent &event)
{
	update_sensor_list();
}

void Rack::CB_UpdateGrid(wxCommandEvent &event)
{
	update_grid();
}

void Rack::set_color(wxPanel *pa, wxStaticText *tx, double data,
    const struct sensor_props *props)
{
	double bg[3], fg[3];

	generate_color(props, data, bg, fg);
	pa->SetBackgroundColour(wxColour(static_cast<int>(bg[0]),
		static_cast<int>(bg[1]), static_cast<int>(bg[2])));
	tx->SetForegroundColour(wxColour(static_cast<int>(fg[0]),
		static_cast<int>(fg[1]), static_cast<int>(fg[2])));
	pa->Refresh();
}

void Rack::update_cell(struct racknode *node, void *sid_v, size_t cell)
{
	/* Update a cell */
	struct HXbtree_node *entry;

	if (node->name == NULL || *node->name == '\0') {
		/* The cell is unused */
		grid_pa[cell]->nodename = NULL;
		grid_pa[cell]->SetBackgroundColour(wxNullColour);
		grid_tx[cell]->SetLabel(wxT("---"));
	} else if ((entry = HXbtree_find(node->sensors, sid_v)) != NULL) {
		/* Cell is used */
		const struct sensor_props *sen =
			HXbtree_get<struct sensor_props *>
			(rack->sensor_tree, sid_v);
		long value = reinterpret_cast<long>(entry->data);
		wxString string;

		if (sen->type == T_VOLTAGE)
			/* We stored the voltage as millivolt */
			string.Printf(wxT("%.2f"), value /= 1000);
		else
			string.Printf(wxT("%.0f"), value);

		grid_pa[cell]->nodename = node->name;
		grid_tx[cell]->SetLabel(string);
		set_color(grid_pa[cell], grid_tx[cell],
			static_cast<double>(value) / 1000, sen);
	} else {
		/* Sensor not found for cell */
		grid_pa[cell]->nodename = node->name;
		grid_pa[cell]->SetBackgroundColour(wxNullColour);
		grid_tx[cell]->SetLabel(wxT("??"));
	}

	return;
}

void Rack::update_grid(void)
{
	/* Update the grid for a given sensor id */
	int i = listbox->GetSelection();

	if (i < 0)
		return;

	void *sid = listbox->GetClientData(i);
	size_t current = 0; /* incremental cell count */

	for (struct racknode **trav = rack->row; *trav != NULL; ++trav) {
		struct racknode *node = *trav;

		for (size_t x = 0; x < rack->width; ++x, ++node, ++current)
			update_cell(node, sid, current);
	}

	Layout();
}

int Rack::update_sensor_list(void)
{
	int i = listbox->GetSelection();
	listbox->Clear();

	for (size_t j = 0; j < rack->num_sensors; ++j) {
		const struct sensor_props *props = rack->sensor_array[j];

		if (props->type > T_NONE && props->type < T_OTHER)
			listbox->Append(wxString(fU8(props->name)),
				props->sid_v);
	}

	if (i >= 0)
		listbox->SetSelection(i);
	return i;
}

BEGIN_EVENT_TABLE(PanelMOV, wxPanel)
	EVT_ENTER_WINDOW(PanelMOV::CB_EnterWindow)
END_EVENT_TABLE()

PanelMOV::PanelMOV(wxPanel *parent, Rack *arg_rack, wxWindowID id) :
    wxPanel(parent, id)
{
	rack_wd  = arg_rack;
	nodename = NULL;
	return;
}

void PanelMOV::CB_EnterWindow(wxMouseEvent &event)
{
	if (rack_wd != NULL && nodename != NULL)
		rack_wd->tx_nodeinfo->SetLabel(wxString(fU8(nodename)));
}
