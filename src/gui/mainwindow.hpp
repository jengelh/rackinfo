#ifndef RACKINFO_MAINWINDOW_HPP
#define RACKINFO_MAINWINDOW_HPP 1

#include <cstdio>
#include <libHX.h>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#	include <wx/wx.h>
#endif
#include <wx/notebook.h>
#include "gui/guiglobal.hpp"

class MainWindow : public wxFrame {
    public:
	MainWindow(const wxString & = wxEmptyString, const wxPoint & = wxDPOS,
		const wxSize & = wxDSIZE);

    private:
	void CB_Exit(wxCommandEvent &);
	void CB_UpdateTabs(wxCommandEvent &);
	void update_sensor_list(void);

	DECLARE_EVENT_TABLE();
};

#endif /* RACKINFO_MAINWINDOW_HPP */
