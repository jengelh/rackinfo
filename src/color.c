/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */
#include "proto.h"
#include "sensor.h"

/**
 * generate_color -
 * @props:	sensor properties
 * @data:	input value
 * @bg:	result background color
 * @fg:	result foreground color
 */
void generate_color(const struct sensor_props *props, double data,
    double *bg, double *fg)
{
	double md = (props->hi_warn + props->lo_warn) / 2;
	double gray;

	if (data <= props->lo_crit) {
		bg[0] = 128;
		bg[1] = 0;
		bg[2] = 128;
	} else if (data <= props->lo_warn) {
		bg[0] = 128.0 * (props->lo_warn - data) /
		        (props->lo_warn - props->lo_crit);
		bg[1] = 0;
		bg[2] = 128;
	} else if (data <= md) {
		bg[0] = 0;
		bg[1] = 255.0 * (data - props->lo_warn) / (md - props->lo_warn);
		bg[2] = 128.0 * (md - data) / (md - props->lo_warn);
	} else if (data <= props->hi_warn) {
		bg[0] = 255.0 * (data - md) / (props->hi_warn - md);
		bg[1] = 255;
		bg[2] = 0;
	} else if (data <= props->hi_crit) {
		bg[0] = 255;
		bg[1] = 255 * (props->hi_crit - data) /
		        (props->hi_crit - props->hi_warn);
		bg[2] = 0;
	} else {
		bg[0] = 255;
		bg[1] = 0;
		bg[2] = 0;
	}

	gray = (77 * bg[0] + 151 * bg[1] + 26 * bg[2]) / 254;
	if (gray < 128) {
		fg[0] = (255.0 + bg[0]) / 2;
		fg[1] = (255.0 + bg[1]) / 2;
		fg[2] = (255.0 + bg[2]) / 2;
	} else {
		fg[0] = bg[0] / 2;
		fg[1] = bg[1] / 2;
		fg[2] = bg[2] / 2;
	}

	return;
}
