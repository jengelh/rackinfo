/*
 *	rackinfo - RAC monitoring
 *	Copyright © Jan Engelhardt <jengelh [at] medozas de>, 2004 - 2009
 *
 *	This file is part of rackinfo.
 *	rackinfo is free software; you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License
 *	or, (at your option), any later version.
 */

/*
 *	html.c - HTML output subsystem
 *	Objectives:
 *		- Create a PHP file which acts like the WX GUI
 */
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <libHX/arbtree.h>
#include <libHX/defs.h>
#include <libHX/deque.h>
#include "loader.h"
#include "proto.h"
#include "qd_int.h"
#include "rack.h"
#include "sensor.h"

static void q_node(FILE *fp, struct racknode *node,
    struct sensor_props *props, void *sid)
{
	struct HXbtree_node *bnode = HXbtree_find(node->sensors, sid);
	unsigned long in = 0;
	double bg[3], fg[3], v;

	if (bnode != NULL) {
		in = (unsigned long)bnode->data;
		v  = in;
	} else {
		fprintf(fp, "\"%s\", array(\"??\"),", node->name);
		return;
	}

	if (props->type == T_VOLTAGE) {
		v /= 1000;
		generate_color(props, v, bg, fg);
		fprintf(fp, "\"%s\", array(%.2f, \"#%02X%02X%02X\", "
		        "\"#%02X%02X%02X\"), ",
		        node->name, v, static_cast(int, bg[0]),
		        static_cast(int, bg[1]), static_cast(int, bg[2]),
		        static_cast(int, fg[0]), static_cast(int, fg[1]),
		        static_cast(int, fg[2]));
	} else {
		generate_color(props, v, bg, fg);
		fprintf(fp, "\"%s\", array(%lu, \"#%02X%02X%02X\", "
		        "\"#%02X%02X%02X\"), ",
		        node->name, in, static_cast(int, bg[0]),
		        static_cast(int, bg[1]), static_cast(int, bg[2]),
		        static_cast(int, fg[0]), static_cast(int, fg[1]),
		        static_cast(int, fg[2]));
	}
}

static void q_rows(FILE *fp, struct rack *rack, struct sensor_props *props)
{
	struct racknode **row = rack->row;
	void *sid = reinterpret_cast(void *, props->sid);

	while (*row != NULL) {
		struct racknode *node = *row;
		size_t x;

		fprintf(fp, "	  array(");

		for (x = 0; x < rack->width; ++x, ++node)
			if (node->name == NULL || *node->name == '\0')
				fprintf(fp, "\"---\", array(), ");
			else
				q_node(fp, node, props, sid);

		fprintf(fp, "),\n");
		++row;
	}
}

static void q_rack(FILE *fp)
{
	struct HXdeque_node *rptr;
	struct rack *rack;

	fprintf(fp, "<?php\n" "$Data = array(\n");

	for (rptr = Racklist->first; rptr != NULL; rptr = rptr->next) {
		struct HXbtree_node *node;
		struct HXbtrav *travp;

		rack = rptr->ptr;
		fprintf(fp, "  \"%s\" => array(\n", rack->name);

		travp = HXbtrav_init(rack->sensor_tree);
		while ((node = HXbtraverse(travp)) != NULL) {
			struct sensor_props *props = node->data;

			if (props == NULL || !(props->type > T_NONE &&
			    props->type < T_OTHER))
				continue;
			fprintf(fp, "	\"%s\" => array(\n", props->name);
			q_rows(fp, rack, props);
			fprintf(fp, "	),\n");
		}
		fprintf(fp, "  ),\n");
	}
	fprintf(fp, ");\n" "include_once(\"webrack.php\"); ?>\n");
}

void *entrypoint(void *argp)
{
	struct subsystem *cpp = argp;
	int x = 0;

	while (true) {
		struct timeval waiter = {QD_ALL_WAIT_TIME, 0};
		FILE *fp = fopen(Opt.outfile, "w");

		if (fp == NULL) {
			fprintf(stderr, "Could not open %s: %s\n",
			        Opt.outfile, strerror(errno));
			if (x == 0)
				/*
				 * Stop html.so if we cannot open the file on
				 * the first try.
				 */
				break;
		}
		++x;
		q_rack(fp);
		fclose(fp);
		select(0, NULL, NULL, NULL, &waiter);
	}

	cpp->exited = true;
	return NULL;
}
