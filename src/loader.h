/*
 *	loader.h - Include file for loader-specific stuff
 *	Objectives:
 *		- subsystem structure
 *		- Optmap structure
 *		- Prototypes for general functions
 */
#ifndef RACKINFO_LOADER_H
#define RACKINFO_LOADER_H 1

#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

struct subsystem {
	const char *library, *symbol;

	/* argument passing */
	void *ptr;
	int argc;
	const char **argv;

	/* private: */
	void *handle;
	void *(*entry)(void *);
	pthread_t id;
	int no_run, exited;
};

struct Optmap {
	int qd_oneshot, qd_verbose;
	char *outfile;
	long interval;
};

extern int ffb_set(char *);

extern struct Optmap Opt;

/* REQUIRED for C++ compiled files */
extern void *entrypoint(void *);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* RACKINFO_LOADER_H */
