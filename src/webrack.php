<?php
/*=============================================================================
rackinfo - RAC monitoring
  Copyright © Jan Engelhardt <jengelh [at] linux01 gwdg de>, 2004 - 2005
  -- License restrictions apply (GPL v2)

  This file is part of rackinfo.
  rackinfo is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; however ONLY version 2 of the License.

  rackinfo is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program kit; if not, write to:
  Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA  02110-1301  USA

  -- For details, see the file named "LICENSE.GPL2"

webrack.php - Web output stub
Objectives:
  - Display $Data as defined in the data file output by the -o option of
    rackinfo
=============================================================================*/

// Selected things
$O_rack = $_GET["rack"];
$O_sensor  = $_GET["sensor"];
?>

<html>
<head>
<title>Rackinfo2 - HTML output</title>
<style type="text/css">
    .raised {
        background-color: #DDDDDD;
        border: 3px outset #DDDDDD;
    }
    a, a:visited {
        color: #0000FF;
    }
    a:active, a:hover, a:focus {
        color: #FF0000;
    }
</style>
</head>

<body>

<table cellspacing="0" cellpadding="3" class="bd"><tr><?php
    foreach($Data as $rack => $d_rack) {
        if($O_rack != "" && $O_rack == $rack) {
            echo "<td bgcolor=\"#DDDDDD\" style=\"border: 3px outset #DDDDDD;\">";
        } else {
            echo "<td bgcolor=\"#DDDDDD\" style=\"border: 3px inset #DDDDDD;\">";
        }
        ?><a href="?rack=<?= $rack ?>"><?= $rack ?></a></td><?php
    }
?></tr></table>

<?php
if($O_rack != "") {
    $d_rack = $Data[$O_rack];

    /* Table start and sensor choise list on the left */ ?>
    <table cellspacing="0" cellpadding="3"><tr>
    <td nowrap="nowrap" valign="top" class="raised">
    <?php

    foreach($d_rack as $sensor => $d_sensor) {
        echo "<a href=\"?rack=${O_rack}&amp;sensor=${sensor}\">$sensor</a><br />";
    }
    ?>
    </td>

    <td align="center" valign="top" width="100%" class="raised">
    <?php // Right side...
    if($O_sensor == "") {
        // No sensor selected, so no grid.
        echo "&nbsp";
    } else {
        echo "<table cellspacing=\"0\" cellpadding=\"2\">";
        $d_sensor = $d_rack[$O_sensor];
        foreach($d_sensor as $row) {
            echo "<tr>";
            for($i = 0; $i < sizeof($row); $i += 2) {
                $node = $row[$i];
                $d_node = $row[$i + 1];
                if($node == "---" || $node == "??") {
                    echo "<td align=\"center\">$node</td>";
                } else {
                    echo "<td align=\"center\" style=\"background-color:";
                    echo "$d_node[1]; color: $d_node[2];\" title=\"$node\">";
                    echo "$d_node[0]</td>";
                }
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    ?>
    </td>
    </tr>
    </table>
<?php
}
?>
</body>
</html>
